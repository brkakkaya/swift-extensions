Pod::Spec.new do |s|
  s.name             = 'BASwiftExtensions'
  s.version          = '0.1.0'
  s.summary          = 'Swift Extensions'


  s.homepage         = 'https://gitlab.com/brkakkaya/swift-extensions'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Burak AKKAYA' => 'brk.akkaya@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/brkakkaya/swift-extensions.git', :tag => s.version.to_s }

 `echo "5.0" > .swift-version`

  s.ios.deployment_target = '10.0'
  s.requires_arc = true
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES' }
  s.static_framework = true
  s.source_files = 'Extension/**/*.swift'
end
