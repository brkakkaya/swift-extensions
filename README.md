# BASwiftExtensions

## Requirements

- iOS 10.0+ / macOS 10.12+ / tvOS 10.0+ / watchOS 3.0+
- Xcode 10.2+
- Swift 5+

### CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For usage and installation instructions, visit their website. To integrate BASwiftExtensions into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'BASwiftExtensions', '~> 0.1.0'
```
