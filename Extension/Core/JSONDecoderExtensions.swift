//
//  JSONDecoderExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation


public extension JSONDecoder {
    convenience init(withDateDecodingStrategy strategy:DateDecodingStrategy) {
        self.init()
        self.dateDecodingStrategy = strategy
    }
}
