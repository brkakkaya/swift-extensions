//
//  FloatExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension Float {
    
    // MARK: - Properties
    var toString: String {
        return "\(self)"
    }
    
    var toInt: Int{
        return Int(self)
    }
    
    //MARK: - Methods
    func isEqual(rhs: Float) -> Bool {
        return abs(self - rhs) < Float.ulpOfOne
    }
}
