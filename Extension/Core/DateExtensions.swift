//
//  DateExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension Date{
    
    // MARK: - Formats
    class Format{
        public static var dd_mm_yyyy: String = "dd.MM.yyyy"
        public static var yyyy_mm_dd_t_hh_mm_ss_z: String = "yyyy-MM-dd'T'HH:mm:ssZ"
        public static var dd_mm_yyyy_hh_mm_ss: String = "dd.MM.yyyy HH:mm:ss"
    }
    
    // MARK: - Initialization
    init?(fromText text: String, withFormat format: String) {
        let dateFormatter = Date.createFormatter(withDateFormat: format)
        guard let date = dateFormatter.date(from: text) else {
            return nil
        }
        
        self = Date(timeInterval: 0, since: date)
    }
    
    // MARK: - Properties
    var year : Int {
        let calendar = Calendar.current
        return calendar.component(.year, from: self)
    }
    
    var month : Int {
        let calendar = Calendar.current
        return calendar.component(.month, from: self)
    }
    
    var day : Int {
        let calendar = Calendar.current
        return calendar.component(.day, from: self)
    }
    
    var hour : Int {
        let calendar = Calendar.current
        return calendar.component(.hour, from: self)
    }
    
    var minute : Int {
        let calendar = Calendar.current
        return calendar.component(.minute, from: self)
    }
    
    var second : Int {
        let calendar = Calendar.current
        return calendar.component(.second, from: self)
    }
    
    // MARK: - Methods
    func toString(withFormat format: String = Format.dd_mm_yyyy) -> String {
        let formatter = Date.createFormatter(withDateFormat: format)
        formatter.locale = Locale(identifier: Locale.current.languageCode ?? "en")
        return formatter.string(from: self)
    }
    
    func add(value:Int, component: Calendar.Component = .minute) -> Date?{
        let calendar = Calendar.current
        return calendar.date(byAdding: component, value: value, to: self)
    }
    
    func getDifference(toDate:Date, asType type: Calendar.Component = .minute) -> Int{
        return Date.getDifference(fromDate: self, toDate: toDate, asType: type)
    }
    
    func getDifference(fromDate:Date, asType type: Calendar.Component = .minute) -> Int{
        return Date.getDifference(fromDate: fromDate, toDate: self, asType: type)
    }
    
    //MARK: - Static Methods
    static func getDifference(fromDate:Date, toDate:Date, asType type: Calendar.Component = .minute) -> Int{
        let calendar = Calendar.current
        return calendar.dateComponents([type], from: fromDate, to: toDate).value(for: type) ?? 0
    }
    
    // MARK: - Private Methods
    fileprivate static func createFormatter(withDateFormat dateFormat: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = dateFormat
        
        return formatter
    }
}
