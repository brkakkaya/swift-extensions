//
//  StringExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public enum QueryError: Error {
    case notValid
}

public extension String {
    
    // MARK: - Properties
    var toInt: Int? {
        return Int(self)
    }
    
    var toDouble: Double?{
        return Double(self)
    }
    
    //MARK: - Comparision Methods
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func isMatch(regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
    
    func removingWhitespace() -> String {
        return self.replacing(" ", replacement: "")
    }
    
    mutating func removeWhitespace(){
        self = self.removingWhitespace()
    }

    func replacing(_ string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: .literal, range: nil)
    }
    
    mutating func replace(_ string: String, replacement: String){
        self = replacing(string, replacement: replacement)
    }
    
    func substring(fromIndex: Int = 0, toIndex: Int? = nil) -> String {
        var to = self.count
        
        if let toIndex = toIndex {
            to = toIndex
        }
        
        if (fromIndex > self.count || to > self.count || fromIndex > to) {
            return ""
        }
        let startIndex = self.index(self.startIndex, offsetBy: fromIndex)
        let endIndex = self.index(self.startIndex, offsetBy: to)
        
        return String(self[startIndex..<endIndex])
    }
    
    func convertQueryStringToDictionary() throws -> [String:String]? {
        let queries = self.split(separator: "&")
        var dict : [String:String] = [:]
        
        try queries.forEach { (query) in
            let splittedQuery = query.split(separator: "=")
            if splittedQuery.count != 2 {
                throw QueryError.notValid
            }
            
            dict[String(splittedQuery[0])] = String(splittedQuery[1])
        }
        
        return dict
    }
    
}
