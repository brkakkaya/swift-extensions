//
//  JSONEncoderExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension JSONEncoder{
    convenience init(withDateEncodingStrategy strategy:DateEncodingStrategy) {
        self.init()
        self.dateEncodingStrategy = strategy
    }
}
