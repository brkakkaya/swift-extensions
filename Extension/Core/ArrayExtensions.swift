//
//  ArrayExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension Array where Element: Equatable {
    
    mutating func remove(contents: [Element]) {
        for element in contents {
            remove(object: element)
        }
    }
    
    mutating func remove(object: Element) {
        if let index = firstIndex(of: object) {
            remove(at: index)
        }
    }
    
    func isEqual(otherArray array: [Element]) -> Bool {
        if self.count != array.count {
            return false
        }
        
        for item in array {
            if !self.contains(item) {
                return false
            }
        }
        
        return true
    }
}
