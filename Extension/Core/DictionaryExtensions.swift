//
//  DictionaryExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 13.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension Dictionary where Key == String{
    func toJSON() -> String?{
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted),
            let rawString = String(data: data, encoding: .utf8) else {
            return nil
        }
        
        return rawString
    }
    
    func toQuery() -> String?{
        var query: String = ""
        
        for (key, value) in self {
            if let valueString = value as? String {
                query.append(key + "=" + valueString + "&")
            }
        }
        
        if query.count > 0 {
            query = String(query.dropLast())
        }
        
        return query.isEmpty ? nil : query
    }
}


