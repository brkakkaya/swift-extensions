//
//  CodableExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension Encodable {
    func jsonData(withDateEncodingStrategy strategy:JSONEncoder.DateEncodingStrategy = .iso8601) throws -> Data {
        return try JSONEncoder(withDateEncodingStrategy: .iso8601).encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8, strategy:JSONEncoder.DateEncodingStrategy = .iso8601) throws -> String? {
        return String(data: try self.jsonData(withDateEncodingStrategy: strategy), encoding: encoding)
    }
    
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
