//
//  NSObjectExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation


public extension NSObject {
    // MARK: - Static Properties
    @objc class var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
