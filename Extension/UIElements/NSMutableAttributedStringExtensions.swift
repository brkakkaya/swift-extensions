//
//  NSMutableAttributedStringExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 3.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public protocol AttributedStringFactoryProtocol{
    
    func getRegularFontName() -> String
    
    func getBoldFontName() -> String
    
    func getRegularFont(ofSize size:CGFloat) -> UIFont
    
    func getBoldFont(ofSize size:CGFloat) -> UIFont
}

open class AttributedStringFactory: AttributedStringFactoryProtocol{

    private let regularFontName:String
    
    private let boldFontName: String
    
    public init(regularFontName:String, boldFontName:String){
        self.regularFontName = regularFontName
        self.boldFontName = boldFontName
    }
    
    public func getBoldFontName() -> String {
        return boldFontName
    }
    
    public func getRegularFontName() -> String {
        return regularFontName
    }
    
    open func getRegularFont(ofSize size: CGFloat) -> UIFont {
        guard let font = UIFont(name: getRegularFontName(), size: size) else{
            print("Font is not created. Font Name:\(getRegularFontName())")
            return UIFont.systemFont(ofSize: size)
        }
        
        return font
    }
    
    open func getBoldFont(ofSize size: CGFloat) -> UIFont {
        guard let font = UIFont(name: getBoldFontName(), size: size) else{
            print("Font is not created. Font Name:\(getBoldFontName())")
            return UIFont.systemFont(ofSize: size)
        }
        
        return font
    }
}

public class AttributedStringBuilder{
    
    private var attributedString: NSMutableAttributedString = NSMutableAttributedString()
    
    private let factory:AttributedStringFactoryProtocol
    
    init(factory:AttributedStringFactoryProtocol) {
        self.factory = factory
    }
    
    func bold(_ text: String, size:CGFloat) -> Self{
        attributedString.attributed(text, font: factory.getBoldFont(ofSize: size))
        return self
    }
    
    func bold(_ text: String, color: UIColor, size:CGFloat) -> Self{
        attributedString.attributed(text, color: color, font: factory.getBoldFont(ofSize: size))
        return self
    }
    
    func boldWithRange(totalText: String, subText: String, size:CGFloat) -> Self{
        attributedString.attributedWithRange(totalText: totalText, subText: subText, font: factory.getBoldFont(ofSize: size))
        return self
    }
    
    func normal(_ text: String, size:CGFloat) -> Self{
        attributedString.attributed(text, font: factory.getRegularFont(ofSize: size))
        return self
    }

    func normal(_ text: String, color: UIColor, size:CGFloat) -> Self{
        attributedString.attributed(text, color: color, font: factory.getRegularFont(ofSize: size))
        return self
    }
    
    func normalWithRange(totalText: String, subText: String, size:CGFloat) -> Self{
        attributedString.attributedWithRange(totalText: totalText, subText: subText, font: factory.getRegularFont(ofSize: size))
        return self
    }
    
    func build() -> NSMutableAttributedString{
        return attributedString
    }
    
}

public extension NSMutableAttributedString {
    
    // MARK: - Methods
    @discardableResult
    func attributed(_ text: String, font:UIFont) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key.font: font]
        let boldString = NSMutableAttributedString(string: "\(text)", attributes: attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult
    func attributed(_ text: String, color: UIColor, font:UIFont) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key.font: font,
                                                          NSAttributedString.Key.foregroundColor: color]
        let normal = NSMutableAttributedString(string: "\(text)", attributes: attrs)
        self.append(normal)
        return self
    }
    
    @discardableResult
    func attributedWithRange(totalText: String, subText: String, font:UIFont) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key.font: font]
        self.addAttributes(attrs, range: (totalText as NSString).range(of: subText))
        return self
    }
}
