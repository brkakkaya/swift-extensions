//
//  UIViewControllerExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    // MARK: - Static Properties
    class var identifier: String {
        return className
    }
    
    func setBackgroundColor(fromImage image: UIImage) {
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
}
