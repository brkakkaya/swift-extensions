//
//  UIViewConstraintExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public extension UIView {
    @discardableResult
    func addHeightConstraint(height: CGFloat) -> NSLayoutConstraint{
        let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute,
                                                  multiplier: 1, constant: height)
        self.addConstraint(heightConstraint)
        return heightConstraint
    }
    
    @discardableResult
    func addWidthConstraint(width: CGFloat) -> NSLayoutConstraint {
        let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute,
                                                 multiplier: 1, constant: width)
        self.addConstraint(widthConstraint)
        return widthConstraint
    }
    
    @discardableResult
    func addTopConstraint(toView: UIView) -> NSLayoutConstraint{
        let topConstraint = self.topAnchor.constraint(equalTo: toView.topAnchor)
        topConstraint.isActive = true
        return topConstraint
    }
    
    
    @discardableResult
    func addTopConstraint(toView: UIView, constant: CGFloat) -> NSLayoutConstraint{
        let topConstraint = self.topAnchor.constraint(equalTo: toView.topAnchor, constant: constant)
        topConstraint.isActive = true
        return topConstraint
    }
    
    @discardableResult
    func addLeadingConstraint(toView: UIView) -> NSLayoutConstraint{
        let leadingConstraint = self.leadingAnchor.constraint(equalTo: toView.leadingAnchor)
        leadingConstraint.isActive = true
        return leadingConstraint
    }
    
    @discardableResult
    func addLeadingConstraint(toView: UIView, constant: CGFloat) -> NSLayoutConstraint{
        let leadingConstraint = self.leadingAnchor.constraint(equalTo: toView.leadingAnchor, constant: constant)
        leadingConstraint.isActive = true
        return leadingConstraint
    }
    
    @discardableResult
    func addTrailingConstraint(toView: UIView) -> NSLayoutConstraint{
        let trailingConstraint = self.trailingAnchor.constraint(equalTo: toView.trailingAnchor)
        trailingConstraint.isActive = true
        return trailingConstraint
    }
    
    @discardableResult
    func addTrailingConstraint(toView: UIView, constant: CGFloat) -> NSLayoutConstraint{
        let trailingConstraint = self.trailingAnchor.constraint(equalTo: toView.trailingAnchor, constant: constant)
        trailingConstraint.isActive = true
        return trailingConstraint
    }
    
    @discardableResult
    func addBottomConstraint(toView: UIView) -> NSLayoutConstraint{
        let bottomConstraint = self.bottomAnchor.constraint(equalTo: toView.bottomAnchor)
        bottomConstraint.isActive = true
        return bottomConstraint
    }
    
    @discardableResult
    func addBottomConstraint(toView: UIView, constant: CGFloat) -> NSLayoutConstraint{
        let bottomConstraint = self.bottomAnchor.constraint(equalTo: toView.bottomAnchor, constant: constant)
        bottomConstraint.isActive = true
        return bottomConstraint
    }
    
    @discardableResult
    func addEqualWidthConstraint(toView: UIView) -> NSLayoutConstraint{
        let constraint = self.widthAnchor.constraint(equalTo: toView.widthAnchor)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult
    func addEqualHeightConstraint(toView: UIView) -> NSLayoutConstraint{
        let constraint = self.heightAnchor.constraint(equalTo: toView.heightAnchor)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult
    func add(toView view: UIView, leading: CGFloat = 0, trailing: CGFloat = 0, top: CGFloat = 0,
             bottom: CGFloat = 0) -> (top:NSLayoutConstraint, leading:NSLayoutConstraint, bottom:NSLayoutConstraint, trailing:NSLayoutConstraint) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        view.translatesAutoresizingMaskIntoConstraints = false
        let leading = self.addLeadingConstraint(toView: view, constant: leading)
        let trailing = self.addTrailingConstraint(toView: view, constant: trailing)
        let top = self.addTopConstraint(toView: view, constant: top)
        let bottom = self.addBottomConstraint(toView: view, constant: bottom)
        
        return (top, leading, bottom, trailing)
    }
}
