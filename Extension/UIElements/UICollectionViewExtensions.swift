//
//  UICollectionViewExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import UIKit

public extension UICollectionView {

    func dequeueCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as? T else {
            fatalError("Cannot dequeue cell with identifier \(T.className)")
        }
        
        return cell
    }
    
    func dequeueReusableView<T: UICollectionReusableView>(_ kind: String, _ indexPath: IndexPath) -> T {
        guard let headerView = self.dequeueReusableSupplementaryView(ofKind: kind,
                                                                     withReuseIdentifier: T.className,
                                                                     for: indexPath) as? T else { fatalError("Cannot dequeue header") }
        
        return headerView
    }
    
    func registerReusableView<T: UICollectionReusableView>(type: T.Type, kind: String) {
        let nib = UINib(nibName: T.className, bundle: nil)
        self.register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.className)
    }
    
    func registerReusableViewFromClass<T: UICollectionReusableView>(type: T.Type, kind: String) {
        self.register(type, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.className)
    }
    
    func registerCell<T: UICollectionViewCell>(type: T.Type) {
        let nib = UINib(nibName: T.className, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: T.className)
    }
    
    func registerCellFromClass<T: UICollectionViewCell>(type: T.Type) {
        self.register(type, forCellWithReuseIdentifier: T.className)
    }
}
