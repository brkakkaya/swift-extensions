//
//  UITableViewExtensions.swift
//  Extension
//
//  Created by Burak Akkaya on 2.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//
import UIKit

public extension UITableView {
    
    func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T else {
            fatalError("Cannot dequeue cell with identifier \(T.className)")
        }
        
        return cell
    }
    
    func registerCell<T: UITableViewCell>(type: T.Type) {
        let nib = UINib(nibName: T.className, bundle: nil)
        self.register(nib, forCellReuseIdentifier: T.className)
    }
    
    func registerCellFromClass<T: UITableViewCell>(type: T.Type) {
        self.register(type, forCellReuseIdentifier: T.className)
    }
}
