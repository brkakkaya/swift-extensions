//
//  Presentable.swift
//  Extension
//
//  Created by Burak Akkaya on 3.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import Foundation

public protocol Presentable {
    func toPresent() -> UIViewController
}

public extension Presentable where Self: UIViewController {
    func toPresent() -> UIViewController {
        return self
    }
}
