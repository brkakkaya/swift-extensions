//
//  DateExtensionTests.swift
//  ExtensionTests
//
//  Created by Burak Akkaya on 3.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import XCTest
@testable import Extension

class DateExtensionTests: XCTestCase {

    func testInitDateWithMatchedFormat_shouldValid(){
        let dateText = "12.09.2018 09:15:38"
        let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss)
        
        XCTAssertNotNil(date)
    }
    
    func testInitDateWithPartiallyMatchedFormat_shouldNotValid(){
        let dateText = "12.09.2018 09:15:38"
        let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy)
        
        XCTAssertNil(date)
    }
    
    func testInitDate_shouldNil(){
        let dateText = "12.09.2018 09:15:38"
        let date = Date(fromText: dateText, withFormat: Date.Format.yyyy_mm_dd_t_hh_mm_ss_z)
        
        XCTAssertNil(date)
    }
    
    func testDateYear(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        XCTAssert(date.year == 2018)
    }
    
    
    func testDateMonth(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        XCTAssert(date.month == 9)
    }
    
    func testDateDay(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        XCTAssert(date.day == 12)
    }
    
    func testDateHour(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        XCTAssert(date.hour == 9)
    }
    
    func testDateMinute(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        XCTAssert(date.minute == 15)
    }
    
    func testDateSecond(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        XCTAssert(date.second == 38)
    }
    
    func testDateToString(){
        // GMT: Thursday, April 12, 2018 1:35:38 PM
        let april_12_2018: Date = Date(timeIntervalSince1970: 1_523_540_138)
        
        XCTAssert(april_12_2018.toString() == "12.04.2018")
    }
    
    func testDateToStringWithFormat(){
        // GMT: Thursday, April 12, 2018 1:35:38 PM
        let april_12_2018: Date = Date(timeIntervalSince1970: 1_523_540_138)
        
        XCTAssert(april_12_2018.toString(withFormat: "MM/yyyy") == "04/2018")
    }

    func testAddMinute(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss)?.add(value: 10) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        
        XCTAssert(date.minute == 25)
    }
    
    func testAddHour(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss)?.add(value: 10, component: .hour) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        
        XCTAssert(date.hour == 19)
    }
    
    func testAddYear(){
        let dateText = "12.09.2018 09:15:38"
        guard let date = Date(fromText: dateText, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss)?.add(value: 10, component: .year) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        
        XCTAssert(date.year == 2028)
    }
    
    func testDifferenceAsMinutes(){
        let dateText1 = "12.09.2018 09:15:38"
        let dateText2 = "12.09.2018 10:15:38"
        
        guard let date1 = Date(fromText: dateText1, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        guard let date2 = Date(fromText: dateText2, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        let diff = date1.getDifference(toDate: date2)
        XCTAssert(diff == 60)
    }
    
    func testDifferenceFromDateAsMinutes(){
        let dateText1 = "12.09.2018 09:15:38"
        let dateText2 = "12.09.2018 10:15:38"
        
        guard let date1 = Date(fromText: dateText1, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        guard let date2 = Date(fromText: dateText2, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        let diff = date1.getDifference(fromDate: date2)
        XCTAssert(diff == -60)
    }
    
    func testDifferenceAsHour(){
        let dateText1 = "12.09.2018 09:15:38"
        let dateText2 = "12.09.2018 10:15:38"
        
        guard let date1 = Date(fromText: dateText1, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        guard let date2 = Date(fromText: dateText2, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        let diff = date1.getDifference(toDate: date2, asType: .hour)
        XCTAssert(diff == 1)
    }
    
    func testDifferenceAsYear(){
        let dateText1 = "12.09.2018 09:15:38"
        let dateText2 = "12.09.2019 09:15:38"
        
        guard let date1 = Date(fromText: dateText1, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        guard let date2 = Date(fromText: dateText2, withFormat: Date.Format.dd_mm_yyyy_hh_mm_ss) else{
            XCTFail("Date didn't initialize")
            return
        }
        
        let diff = Date.getDifference(fromDate: date1, toDate: date2, asType: .year)
        XCTAssert(diff == 1)
    }
    
}
