//
//  StringExtensionTests.swift
//  ExtensionTests
//
//  Created by Burak Akkaya on 3.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import XCTest

class StringExtensionTests: XCTestCase {

    func testToInt_shouldValid(){
        let value = "3".toInt
        XCTAssert(value != nil && value! == 3)
    }
    
    func testToInt_shouldNotValid(){
        let value = "3.2".toInt
        XCTAssertNil(value)
    }
    
    func testToDouble_shouldValid(){
        let value = "3.2".toDouble
        XCTAssert(value != nil && value!.isEqual(rhs: 3.2))
    }
    
    func testToDoubleWithoutFractional_shouldValid(){
        let value = "3".toDouble
        XCTAssert(value != nil && value!.isEqual(rhs: 3.0))
    }
    
    func testToDouble_shouldNotValid(){
        let value = "3.2as".toDouble
        XCTAssertNil(value)
    }
    
    func testCapitalizeFirstLetter(){
        let value = "aaaaaaaa"
        XCTAssert(value.capitalizingFirstLetter() == "Aaaaaaaa")
    }
    
    func testCapitalizeFirstLetterEmptyString(){
        let value = ""
        XCTAssert(value.capitalizingFirstLetter() == "")
    }
    
    func testMutatingCapitalizeFirstLetter(){
        var value = "burak"
        value.capitalizeFirstLetter()
        
        XCTAssert(value == "Burak")
    }
    
    func testRegex_shouldTrue(){
        let regex = "^[^<>!@#$%^&*()0-9]*$"
        let value = "Burak"
        XCTAssertTrue(value.isMatch(regex: regex))
    }
    
    func testRegex_shouldFalse(){
        let regex = "^[^<>!@#$%^&*()0-9]*$"
        let value = "Burak12"
        XCTAssertFalse(value.isMatch(regex: regex))
    }
    
    func testRemoveWhitespace(){
        var value = "Swift Programming Language"
        value.removeWhitespace()
        
        XCTAssert(value == "SwiftProgrammingLanguage")
    }
    
    func testReplace(){
        var value = "Swift Programming Language"
        value.replace("Swift", replacement: "Objective C")
        
        XCTAssert(value == "Objective C Programming Language")
    }
    
    func testReplaceNotContainsSubstring(){
        var value = "Swift Programming Language"
        value.replace("Objective C", replacement: "Java")
        
        XCTAssert(value == "Swift Programming Language")
    }
    
    func testSubstringFromIndexToIndex(){
        let value = "Swift Programming Language"
        let substring = value.substring(fromIndex: 6, toIndex: 17)
        XCTAssert(substring == "Programming")
    }
    
    func testSubstringFromIndexToEnd(){
        let value = "Swift Programming Language"
        let substring = value.substring(fromIndex: 6)
        XCTAssert(substring == "Programming Language")
    }
    
    func testSubstringFromStartToIndex(){
        let value = "Swift Programming Language"
        let substring = value.substring(toIndex: 5)
        XCTAssert(substring == "Swift")
    }
    
    func testSubstringOutOfRange(){
        let value = "Swift Programming Language"
        let substring = value.substring(toIndex: 40)
        XCTAssert(substring == "")
    }

    
    func testQueryStringToDictionary(){
        let query = "name=Burak&surname=Akkaya"
        let dict = try? query.convertQueryStringToDictionary()
        
        XCTAssert(dict != nil && dict!["name"] == "Burak" && dict!["surname"] == "Akkaya")
    }
    
    func testNotValidQueryStringToDictionary(){
        let query = "name=Burak&surname"
        
        XCTAssertThrowsError(try query.convertQueryStringToDictionary())
    }
}
