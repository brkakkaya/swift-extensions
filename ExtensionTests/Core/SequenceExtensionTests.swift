//
//  SequenceExtensionTests.swift
//  ExtensionTests
//
//  Created by Burak Akkaya on 4.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import XCTest

class TestObject:Hashable{
    var key:String
    var value: String

    init(key:String, value:String) {
        self.key = key
        self.value = value
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.key)
    }
    
    static func == (lhs: TestObject, rhs: TestObject) -> Bool {
        return lhs.key == rhs.key && lhs.value == rhs.value
    }
}

class SequenceExtensionTests: XCTestCase {

    func testGroupBy(){
        let list = [TestObject(key: "Apple", value: "iPhone6"),
                    TestObject(key: "Apple", value: "iPhone7"),
                    TestObject(key: "Apple", value: "iPhone8"),
                    TestObject(key: "Apple", value: "iPhoneX"),
                    TestObject(key: "Samsung", value: "S4"),
                    TestObject(key: "Samsung", value: "S8"),
                    TestObject(key: "Samsung", value: "S9")]
    
        let result = list.group(by: { $0.key })
        
        
        XCTAssert(result["Apple"]?.count  == 4)
    }

}
