//
//  IntegerExtensionTests.swift
//  ExtensionTests
//
//  Created by Burak Akkaya on 3.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import XCTest
@testable import Extension

class IntegerExtensionTests: XCTestCase {

    func testToString(){
        let value:Int = 3
        XCTAssert(value.toString == "3")
    }

}
