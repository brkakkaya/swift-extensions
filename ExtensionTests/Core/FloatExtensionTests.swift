//
//  FloatExtensionTests.swift
//  ExtensionTests
//
//  Created by Burak Akkaya on 4.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import XCTest

class FloatExtensionTests: XCTestCase {

    func testToString(){
        let value:Float = 3.5
        XCTAssert(value.toString == "3.5")
    }
    
    func testToInt(){
        let value:Float = 3.5
        XCTAssert(value.toInt == 3)
    }
    
    func testEqualWithEpsilon_shouldTrue(){
        let value1: Float = 3.000000000000000005
        let value2: Float = 3.000000000000000004
        
        XCTAssert(value1.isEqual(rhs: value2))
    }
    
    func testEqual_shouldTrue(){
        let value1: Float = 3.00004
        let value2: Float = 3.00004
        
        XCTAssert(value1.isEqual(rhs: value2))
    }
    
    func testEqual_shouldFalse(){
        let value1: Float = 3.00004
        let value2: Float = 3.00003
        
        XCTAssertFalse(value1.isEqual(rhs: value2))
    }

}
