//
//  ArrayExtensionTests.swift
//  ExtensionTests
//
//  Created by Burak Akkaya on 4.05.2019.
//  Copyright © 2019 Burak Akkaya. All rights reserved.
//

import XCTest

class ArrayExtensionTests: XCTestCase {

    func testRemoveElement(){
        var values : [String] = ["Swift", "Objective C", "Java", ".Net", "Phyton"]
        values.remove(object: "Swift")
        XCTAssert(values.isEqual(otherArray: ["Objective C", "Java", ".Net", "Phyton"]))
    }

    func testRemoveElements(){
        var values : [String] = ["Swift", "Objective C", "Java", ".Net", "Phyton"]
        values.remove(contents: ["Swift", "Objective C"])
        XCTAssert(values.isEqual(otherArray: ["Java", ".Net", "Phyton"]))
    }
    
    func testIsEqual_shouldTrue(){
        let values : [String] = ["Swift", "Objective C", "Java", ".Net", "Phyton"]
        XCTAssert(values.isEqual(otherArray: ["Objective C", "Phyton", "Java", "Swift", ".Net"]))
    }
    
    func testIsEqual_shouldFalse(){
        let values : [String] = ["Swift", "Objective C", "Java", ".Net", "Phyton"]
        XCTAssertFalse(values.isEqual(otherArray: ["Objective C", "Phyton", "Java", "Swift", ".Net", "Spring"]))
    }
}
